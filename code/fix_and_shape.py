#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Student: Peter Bakke
# Project: Fix Street names and create CSV tables for use importing to SQLite3

import csv
import codecs
import pprint
import re
import xml.etree.cElementTree as ET
import cerberus
import schema
import pandas as pd
import os
import imp


OSM_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\newport_ri_USA_and_vicinity.osm"
#OSM_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\sample.osm"

NODES_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\nodes.csv"
NODE_TAGS_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\nodes_tags.csv"
WAYS_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\ways.csv"
WAY_NODES_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\ways_nodes.csv"
WAY_TAGS_PATH = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\ways_tags.csv"

LOWER_COLON = re.compile(r'^([a-z]|_)+:([a-z]|_)+')
PROBLEMCHARS = re.compile(r'[=\+/&<>;\'"\?%#$@\,\. \t\r\n]')

SCHEMA = schema.schema

# Make sure the fields order in the csvs matches the column order in the sql table schema
NODE_FIELDS = ['id', 'lat', 'lon', 'user', 'uid', 'version', 'changeset', 'timestamp']
NODE_TAGS_FIELDS = ['id', 'key', 'value', 'type']
WAY_FIELDS = ['id', 'user', 'uid', 'version', 'changeset', 'timestamp']
WAY_TAGS_FIELDS = ['id', 'key', 'value', 'type']
WAY_NODES_FIELDS = ['id', 'node_id', 'position']


street_type_re = re.compile(r'\b\S+\.?$', re.IGNORECASE)

# Import lengthy 'expected' and 'mapping' list/dict from local source file 'projectvars.txt'
# ...this was done for easier readability & less scrolling through code. It took considerable time to figure this out, 
# but it will be worthwhile in future projects to be able to avoid including large source data files in the code
mod = imp.load_source('projvars', '.\data\projectvars.txt')
from projvars import expected, mapping


# Check if tag keyword indicates OSM address
def is_street_name(elem):
    return (elem.attrib['k'] == "addr:street") 


# All 'fix' element functions are defined here
def check_elem(elem):
    
    # Check/Fix Street Name:
    def fix_street(elem):
 
        if elem.tag == "node" or elem.tag == "way":
 
            for tag in elem.iter("tag"):
 
                if is_street_name(tag):

                    street_name = tag.attrib['v']  # Full street name
                
                    m = street_type_re.search(street_name)
                    
                    if m:
                        
                        street_type = m.group()                  ### Assign street type 
                        street_type.rstrip('.')                  ### Strip any ending period
                        street_type_upper = street_type.upper()  ### 'expected' and 'mapping' objects are all uppercase
                        
                        if street_type_upper not in expected:
                                        
                            if street_type_upper in mapping:
                                    
                                new_name = street_name.replace(street_type, mapping[street_type_upper])
                                            
                                if new_name != street_name:
                                    #print "Fixed:", tag.attrib['v'], "=>", new_name
                                    tag.attrib['v'] = new_name
                                    return
                                                    
                            
        # END fix_street(elem)          
            
    fix_street(elem)  #Only one 'fix' function defined in this project, but when adding others, call them all here.
    
    return  # END def check_elem(elem)


def shape_element(element, node_attr_fields=NODE_FIELDS, way_attr_fields=WAY_FIELDS,
                  problem_chars=PROBLEMCHARS, default_tag_type='regular'):
                      
    """ Clean and shape node or way XML element to Python dict """

    node_attribs = {}
    way_attribs = {}
    way_nodes = []
    tags = []  # Handle secondary tags the same way for both node and way elements
    
    #Check if element needs fixing
    check_elem(element)
    
    
    # Shape data for importing to SQLite3
    if element.tag == 'node':
        
        subnode_attribs = {}
        
        for items in element.iter('node'):   #Get Node 
            
            for key in items.attrib:    # loop thru node attributes
                if key == 'visible':
                    pass
                elif key == 'id':
                    node_attribs[key] = items.attrib[key]
                    nodeid = items.attrib[key]  # save for use belowin tag processing
                else:
                    node_attribs[key] = items.attrib[key]

        
        #################### Next: TAGS


        for tag in element.iter('tag'):
            subnode_tags = {}
            subnode_tags['id'] = nodeid
            subnode_tags['key'] = tag.attrib['k']
            subnode_tags['value'] = tag.attrib['v']
            subnode_tags['type'] = 'regular'
            
            tags.append(subnode_tags)
            
            
        
        return {'node': node_attribs, 'node_tags': tags}
    
    
        
    elif element.tag == 'way':
        
        subway_attribs = {}
        
        for items in element.iter('way'):   #Get Way 
            
            for key in items.attrib:    # loop thru WAY attributes
                if key == 'visible':
                    pass
                else:
                    subway_attribs[key] = items.attrib[key]

        ## In this case set 'node' key to dict created above
        way_attribs['way'] = subway_attribs
        
        #################### Next: WAY NODES

        pos = 0
        for tag in element.iter('nd'):
            subway_node = {}
            
            subway_node['id'] = subway_attribs['id']
            subway_node['node_id'] = tag.attrib['ref']
            subway_node['position'] = pos
            pos += 1

            way_nodes.append(subway_node)

        
        
        #################### Next: TAGS

        for tag in element.iter('tag'):
            subway_tags = {}
            subway_tags['id'] = subway_attribs['id']
            subway_tags['key'] = tag.attrib['k']
            subway_tags['value'] = tag.attrib['v']
            
            
            
            subway_tags['type'] = 'regular'
            

            tags.append(subway_tags)

        

        way_attribs['way'] = subway_attribs
        way_attribs['way_tags'] = tags
        way_attribs['way_nodes'] = way_nodes
 
        
        
        return {'way': subway_attribs, 'way_nodes': way_nodes, 'way_tags': tags}        


# ================================================== #
#        Helper Functions provided by Udacity        #
# ================================================== #
def get_element(osm_file, tags=('node', 'way', 'relation')):
    """Yield element if it is the right type of tag"""

    context = ET.iterparse(osm_file, events=('start', 'end'))
    _, root = next(context)
    for event, elem in context:
        if event == 'end' and elem.tag in tags:
            yield elem   ###  This is a special function that retains local state data, ie its place/pointer in the file
            root.clear()


def validate_element(element, validator, schema=SCHEMA):
    """Raise ValidationError if element does not match schema"""
    if validator.validate(element, schema) is not True:
        field, errors = next(validator.errors.iteritems())
        message_string = "\nElement of type '{0}' has the following errors:\n{1}"
        error_string = pprint.pformat(errors)
        
        raise Exception(message_string.format(field, error_string))


class UnicodeDictWriter(csv.DictWriter, object):
    """Extend csv.DictWriter to handle Unicode input"""

    def writerow(self, row):
        super(UnicodeDictWriter, self).writerow({
            k: (v.encode('utf-8') if isinstance(v, unicode) else v) for k, v in row.iteritems()
        })

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


# ================================================== #
#   Main csv Write Function provided by Udacity      #
# ================================================== #
def process_map(file_in, validate):
    """Iteratively process each XML element and write to csv(s)"""

    with codecs.open(NODES_PATH, 'w') as nodes_file, \
        codecs.open(NODE_TAGS_PATH, 'w') as nodes_tags_file, \
         codecs.open(WAYS_PATH, 'w') as ways_file, \
         codecs.open(WAY_NODES_PATH, 'w') as way_nodes_file, \
         codecs.open(WAY_TAGS_PATH, 'w') as way_tags_file:

        nodes_writer = UnicodeDictWriter(nodes_file, NODE_FIELDS, lineterminator = '\n')
        node_tags_writer = UnicodeDictWriter(nodes_tags_file, NODE_TAGS_FIELDS, lineterminator = '\n')
        ways_writer = UnicodeDictWriter(ways_file, WAY_FIELDS, lineterminator = '\n')
        way_nodes_writer = UnicodeDictWriter(way_nodes_file, WAY_NODES_FIELDS, lineterminator = '\n')
        way_tags_writer = UnicodeDictWriter(way_tags_file, WAY_TAGS_FIELDS, lineterminator = '\n')

        nodes_writer.writeheader()
        node_tags_writer.writeheader()
        ways_writer.writeheader()
        way_nodes_writer.writeheader()
        way_tags_writer.writeheader()

        validator = cerberus.Validator()

        for element in get_element(file_in, tags=('node', 'way')):
            
            
            
            
            el = shape_element(element)  ##################################
            
            
            if el:
                if validate is True:
                    #print '\n\nELEMENT : ', el, "\n\n"
                    validate_element(el, validator)

                # Write to the 5 files that will be imported as tables to SQLite
                if element.tag == 'node':
                    nodes_writer.writerow(el['node'])
                    node_tags_writer.writerows(el['node_tags'])
                elif element.tag == 'way':
                    ways_writer.writerow(el['way'])
                    way_nodes_writer.writerows(el['way_nodes'])
                    way_tags_writer.writerows(el['way_tags'])
            

if __name__ == '__main__':

    process_map(OSM_PATH, validate=True)
    print "Done ..."
