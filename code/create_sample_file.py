#!/usr/bin/env python
# -*- coding: utf-8 -*-

####  Create Sample OSM file from larger OSM datafile (see xyz.py)

import xml.etree.ElementTree as ET  # Use cElementTree or lxml if too slow
# Replace this with your osm file
OSM_FILE = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\newport_ri_USA_and_vicinity.osm"  
SAMPLE_FILE = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\sample.osm"

k = 10 # Parameter: take every k-th top level element

def get_element(osm_file, tags=('node', 'way', 'relation')):
    """Yield element if it is the right type of tag

    Reference:
    http://stackoverflow.com/questions/3095434/inserting-newlines-in-xml-file-generated-via-xml-etree-elementtree-in-python
    """
    context = iter(ET.iterparse(osm_file, events=('start', 'end')))
    _, root = next(context)
    for event, elem in context:   ### Pulls entire element, all tags, everything for that element
        if event == 'end' and elem.tag in tags:
            yield elem      # This 'indicates' this func remembers where it is pointing in the read file
            root.clear()


with open(SAMPLE_FILE, 'wb') as output:
    output.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    output.write('<osm>\n  ')

    # Write every kth top level element
    for i, element in enumerate(get_element(OSM_FILE)):
        if i % k == 0:
            output.write(ET.tostring(element, encoding='utf-8'))

    output.write('</osm>')
    
    print "Done!"

