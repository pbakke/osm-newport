#### Print out zip code variations for inspection / correction ... 


import os
import xml.etree.cElementTree as ET
from collections import defaultdict
import re
from pprint import pprint 


DATADIR = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\"
#DATAFILE = "newport_ri_USA_and_vicinity.osm"

DATAFILE = "sample.osm"

datafile = os.path.join(DATADIR, DATAFILE)


with open(datafile, "rb") as osm_file:


    postcodes = defaultdict(int)   # like street_types = {}   ... only can supply type [ie, int, float, str, etc]?

    def audit_postcode(postcode):
        postcodes[postcode] += 1
    ### END audit_street_type

    def print_sorted_dict(d):
        keys = d.keys()
        print 'keys = ', keys, '\n\n'
        
        keys = sorted(keys)
        for k in keys:
            v = d[k]
            print "%s: %d" % (k, v) 
    ### END print_sorted_dict(d)

    def is_postcode(elem):
        return (elem.tag == "tag") and ((elem.attrib['k'] == "addr:postcode") or (elem.attrib['k'] == "tiger:zip_left") or (elem.attrib['k'] == "tiger:zip_right"))

    def audit():
        for event, elem in ET.iterparse(osm_file):
            if is_postcode(elem):
                audit_postcode(elem.attrib['v'])   
            
        print_sorted_dict(postcodes)    
        print '\n\n\n'
    

    
    ### END audit()
    
    
    if __name__ == '__main__':
        audit()