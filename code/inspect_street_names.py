#### Print out street name variations for inspection   
# Also look for problem characters


import os
import xml.etree.cElementTree as ET
from collections import defaultdict
import re
from pprint import pprint 


DATADIR = "C:\\Users\\peter\\Documents\\GIT\\Udacity\\data\\"
#DATAFILE = "NewportRI.osm.xml"

DATAFILE = "sample.osm"

datafile = os.path.join(DATADIR, DATAFILE)


problemchars_re = re.compile(r'[=\+/&<>;"\?%#$@]\t\r\n')


#Check for any problem chars in Street names.

with open(datafile, "rb") as osm_file:

    street_type_re = re.compile(r'\S+\.?$', re.IGNORECASE)  # Gets the LAST word in the string... street, in this case, below
    street_types = defaultdict(int)   # like street_types = {}   ... only can supply type [ie, int, float, str, etc]?

    def audit_street_type(street_types, street_name):
        m = street_type_re.search(street_name)
        if m:
            street_type = m.group()
            street_types[street_type] += 1
            
        x = problemchars_re.search(street_name)
        if x:
            problem = x.group()
            street_types[problem] += 1
            print 'Found problem char = ', problem

    ### END audit_street_type

    def print_sorted_dict(d):
        keys = d.keys()
        #print 'keys = ', keys, '\n\n'
        keys = sorted(keys)
        for k in keys:
            v = d[k]
            print "%s: %d" % (k, v) 
    ### END print_sorted_dict(d)

    def is_street_name(elem):
        return (elem.tag == "tag") and (elem.attrib['k'] == "addr:street") 

    def audit():
        for event, elem in ET.iterparse(osm_file):
            if is_street_name(elem):
                audit_street_type(street_types, elem.attrib['v'])   
            
        print_sorted_dict(street_types)    
        print '\n\n\n'
    

    
    ### END audit()
    
    
    if __name__ == '__main__':
        audit()